const express = require('express');
const app = express();
var cors = require('cors')
const controller = require('./src/module/user')
require('dotenv').config()

app.use(cors())

//Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
var mongoDB = 'mongodb://localhost:27017/email_verification';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// parse body params and attache them to req.body
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//route untuk halaman home
app.get('/', (req, res) => {
  res.send('This is about page');
});
app.post('/api/v1/register', controller.signup);
app.post('/api/v1/login', controller.login);
app.post('/api/v1/profile', controller.profile);
app.post('/api/v1/resend-link/', controller.resendLink);
app.get('/api/v1/confirmation/:email/:token',controller.confirmEmail)
app.post('/api/v1/forgot-password',controller.forgotPassword)
app.post('/api/v1/reset-password',controller.resetPassword)

app.listen(8000, () => {
  console.log('Server is running at port 8000');
});
