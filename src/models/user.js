const mongoose = require("mongoose");

var userSchema = new mongoose.Schema({
  name: String,
  email: { type: String, unique: true },
  isVerified: { type: Boolean, default: false },
  password: String,
}); 

module.exports = mongoose.model('User', userSchema);
